<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ORM\DB;
use SilverStripe\Control\Email\Email;
use Hestec\WlabelMobile\WlabelMobileSubscription;
use Hestec\WlabelMobile\WlabelMobileSupplier;
use SilverStripe\Core\Config\Config;

class WlabelMobileCron extends \SilverStripe\Control\Controller {

    private static $allowed_actions = array (
        'UpdateWlabelMobile',
        'test'
    );

    public function test(){

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            /*$whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://api-internet.whitelabeled.nl/v1/compare/'.$whitelabel_id.'?include_tv=true&include_phone=true&page=1&items_per_page=1', [
                'curl' => [
                    CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                ]
            ]);
            $msgs = json_decode($response->getBody());


            foreach ($msgs->products as $node) {

                //echo $node->score_elements;

                foreach ($node->score_elements as $node) {

                    echo $node->type;

                }

            }*/

            //$email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "New supplier added for Whitelabeled Mobile", "New supplier added for Whitelabeled Mobile");
            //$email->sendPlain();

            $string = "hollandsnieuwe-sim-only-2000";
            $startString = "Hollandsnieuwe";

            $len = strlen($startString);
            if (substr(strtolower($string), 0, $len) === strtolower($startString)){
                return substr($string, $len+1);
            }
            return $string;



        }else{

            return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

        }

    }

    public function UpdateWlabelMobile() {

        if (in_array($_SERVER['REMOTE_ADDR'], Config::inst()->get('AdminTasks', 'allowed_ips'))) {

            $whitelabel_id = Config::inst()->get(__CLASS__, 'whitelabel_id');

            $client = new \GuzzleHttp\Client();
            try {
                $response = $client->request('GET', 'https://api-mobile.whitelabeled.nl/v1/compare/' . $whitelabel_id . '?page=1&items_per_page=999', [
                    'curl' => [
                        CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
                    ]
                ]);

            } catch (Exception $e) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "Error at update Whitelabeled Mobile", $e->getMessage());
                $email->sendPlain();
                $error = true;
            }
            if (!isset($error)) {

                $msgs = json_decode($response->getBody());

                DB::query("UPDATE WlabelMobileSubscription SET StillInApi = 0");

                foreach ($msgs->products as $node) {

                    $count = WlabelMobileSubscription::get()->filter('BaseId', $node->base_id)->count();

                    if ($count == 0) {

                        $sub = new WlabelMobileSubscription();

                    } else {

                        $sub = WlabelMobileSubscription::get()->filter('BaseId', $node->base_id)->first();

                    }

                    // if the base_name starts with the provider_name, remove it
                    $basename = $this->removeProviderName($node->base_name, $node->provider_name);

                    $sub->UrlId = SiteTree::create()->generateURLSegment($node->provider_name . "-" . $basename . "-" . $node->base_id);
                    $sub->ShortUrlId = SiteTree::create()->generateURLSegment($node->base_id . "-" . $basename);
                    $sub->StillInApi = 1; // see the DB::query above
                    $sub->BaseId = $node->base_id;
                    $sub->BaseName = $basename;
                    $sub->Provider = $node->provider;
                    $sub->ProviderName = $node->provider_name;
                    $sub->BasePrice = $node->base_price;
                    $sub->BasePriceInitial = $node->base_price_initial;
                    $sub->DiscountPrice = $node->discount_price;
                    $sub->DiscountPriceInitial = $node->discount_price_initial;
                    $sub->DiscountDuration = $node->discount_duration;
                    $sub->TotalPriceInitial = $node->total_price_initial;
                    $sub->TotalPrice = $node->total_price;
                    $sub->TotalPriceDiscounted = $node->total_price_discounted;
                    $sub->TotalPriceContract = $node->total_price_contract;
                    $sub->ComparePrice = $node->compare_price;
                    $sub->ContractDuration = $node->contract_duration;
                    $sub->ContractDurationText = $node->contract_duration_text;
                    $sub->OfferText = $node->offer;
                    $sub->Network = $node->network;
                    $sub->NetworkType = $node->network_type;
                    $sub->NetworkSpeed = $node->network_speed;
                    $sub->Minutes = $node->minutes;
                    $sub->Internet = $node->data;
                    $sub->Sms = $node->texts;
                    $sub->SharedBundle = $node->shared_bundle;
                    $sub->SharedBundleData = $node->shared_bundle_data;
                    $sub->Usp = $node->usp;
                    $sub->Score = $node->score_elements;
                    $sub->SignupUrl = $node->signup_url;

                    $votes = rand(0, 2);
                    $score = rand(7, 10) * $votes;
                    $sub->RatingVotes = $sub->RatingVotes + $votes;
                    $sub->RatingScore = $sub->RatingScore + $score;

                    if ($supplier = WlabelMobileSupplier::get()->filter('SystemName', $node->provider)->first()) {

                        $sub->WlabelMobileSupplierID = $supplier->ID;

                    } else {

                        $supplier = new WlabelMobileSupplier();
                        $supplier->SystemName = $node->provider;
                        $supplier->Name = $node->provider_name;
                        $supplier->write();

                        $sub->WlabelMobileSupplierID = $supplier->ID;

                        $email = new Email(Config::inst()->get('AdminTasks', 'NotifySender'), Config::inst()->get('AdminTasks', 'NotifyRecipient'), "New supplier added for Whitelabeled Mobile", "The supplier ".$node->provider_name." is added for Whitelabeled Mobile");
                        $email->sendPlain();

                    }

                    $sub->write();

                }
            }

            return "updated";

        }

        return "no valid ip, add the ip(s) in your mysite.yml, see the readme.";

    }

    public function removeProviderName($string, $startString){

        $len = strlen($startString);
        if (substr(strtolower($string), 0, $len) === strtolower($startString)){
            return substr($string, $len+1);
        }
        return $string;

    }

}
