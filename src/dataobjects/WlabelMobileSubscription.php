<?php

namespace Hestec\WlabelMobile;

use SilverStripe\ORM\DataObject;

class WlabelMobileSubscription extends DataObject {

    private static $singular_name = 'WlabelMobileSubscription';
    private static $plural_name = 'WlabelMobileSubscriptions';

    private static $table_name = 'WlabelMobileSubscription';

    private static $db = array(
        'BaseId' => 'Int',
        'BaseName' => 'Varchar(255)',
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)',
        'Provider' => 'Varchar(50)',
        'ProviderName' => 'Varchar(50)',
        'BasePrice' => 'Currency',
        'BasePriceInitial' => 'Currency',
        'DiscountPrice' => 'Currency',
        'DiscountPriceInitial' => 'Currency',
        'DiscountDuration' => 'Int',
        'TotalPriceInitial' => 'Currency',
        'TotalPrice' => 'Currency',
        'TotalPriceDiscounted' => 'Currency',
        'TotalPriceContract' => 'Currency',
        'ComparePrice' => 'Currency',
        'ContractDuration' => 'Int',
        'ContractDurationText' => 'Varchar(20)',
        'OfferText' => 'Text',
        'Network' => 'Varchar(20)',
        'NetworkType' => 'Varchar(10)',
        'NetworkSpeed' => 'Int',
        'Minutes' => 'Int',
        'Internet' => 'Int',
        'Sms' => 'Int',
        'SharedBundle' => 'Boolean',
        'SharedBundleData' => 'Boolean',
        'Usp' => 'MultiValueField',
        'Score' => 'MultiValueField',
        'RatingVotes' => 'Int',
        'RatingScore' => 'Int',
        'SignupUrl' => 'Varchar(255)',
        'StillInApi' => 'Boolean',
        'ProductPage' => 'Boolean'
    );

    private static $has_one = array(
        //'ComparisonAllInOnePage' => ComparisonAllInOnePage::class,
        'WlabelMobileSupplier' => WlabelMobileSupplier::class
    );

    public function MbDownloadSpeed(){

        $mb = round($this->DownloadSpeed / 1000);
        return floor($mb/5) * 5;

    }

    public function InternetNice(){

        if ($this->Internet < 1000){
            return $this->Internet." MB";
        }else{
            return ($this->Internet / 1000)." GB";
        }

    }

    public function PriceEuro($price){

        $output = number_format($price, 2, ',', '');

        return $output;

    }

    public function RatingAverageScore(){

        $output = round($this->RatingScore / $this->RatingVotes, 1);
        return number_format($output, 1, '.', '');

    }

    public function onBeforeWrite()
    {

        if ($this->ID && $this->isChanged('UrlId')){

            $old = WlabelMobileSubscription::get()->byID($this->ID);

            $add = new WlabelMobileOldUrl();
            $add->UrlId = $old->UrlId;
            $add->ShortUrlId = $old->ShortUrlId;
            $add->WlabelMobileSubscriptionID = $this->ID;
            $add->write();

            $this->ProductPage = 0;

        }

        parent::onBeforeWrite();

    }

}