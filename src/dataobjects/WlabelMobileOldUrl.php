<?php

namespace Hestec\WlabelMobile;

use SilverStripe\ORM\DataObject;

class WlabelMobileOldUrl extends DataObject {

    private static $singular_name = 'WlabelMobileOldUrl';
    private static $plural_name = 'WlabelMobileOldUrls';

    private static $table_name = 'WlabelMobileOldUrl';

    private static $db = array(
        'UrlId' => 'Varchar(255)',
        'ShortUrlId' => 'Varchar(255)'
    );

    private static $has_one = array(
        'WlabelMobileSubscription' => WlabelMobileSubscription::class
    );

}