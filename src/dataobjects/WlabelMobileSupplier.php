<?php

namespace Hestec\WlabelMobile;

use SilverStripe\ORM\DataObject;

class WlabelMobileSupplier extends DataObject {

    private static $singular_name = 'WlabelMobileSupplier';
    private static $plural_name = 'WlabelMobileSuppliers';

    private static $table_name = 'WlabelMobileSupplier';

    private static $db = array(
        'SystemName' => 'Varchar(50)',
        'Name' => 'Varchar(50)'
    );

    /*private static $has_one = array(
        'SupplierPage' => SupplierPage::class
    );*/

    private static $has_many = array(
        'WlabelMobileSubscriptions' => WlabelMobileSubscription::class
    );

    /*private static $summary_fields = array(
        'Title',
        'StartDate.Nice',
        'EndDate.Nice',
        'Enabled.Nice'
    );*/

}